package org.bokoch.TelegramBot.Commands;


import org.telegram.telegrambots.api.objects.Chat;
import org.telegram.telegrambots.api.objects.User;
import org.telegram.telegrambots.bots.AbsSender;
import org.telegram.telegrambots.bots.commandbot.commands.BotCommand;

public class QuizStartCommand extends BotCommand {
    public static final String LOGTAG = QuizStartCommand.class.getName();

    public QuizStartCommand() {
        super("startQuiz", "Start taking the test");
    }


    @Override
    public void execute(AbsSender absSender, User user, Chat chat, String[] strings) {

    }
}
