package org.bokoch.TelegramBot;

import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;
import org.telegram.telegrambots.logging.BotsFileHandler;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

public class MainBot {
        private static final String LOGTAG = MainBot.class.getName();

        public static void main(String[] args) {
            BotLogger.setLevel(Level.ALL);
            BotLogger.registerLogger(new ConsoleHandler());
            try {
                BotLogger.registerLogger(new BotsFileHandler());
            } catch (IOException e) {
                BotLogger.severe(LOGTAG, e);
            }

            ApiContextInitializer.init();
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            try {
                telegramBotsApi.registerBot(new CommandsHandler("MYSORROSUM_bot"));
            } catch (TelegramApiException e) {
                BotLogger.warn(LOGTAG, e);
            }
        }
}


