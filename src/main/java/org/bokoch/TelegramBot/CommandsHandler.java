package org.bokoch.TelegramBot;


import org.bokoch.TelegramBot.Commands.HelpCommand;
import org.bokoch.TelegramBot.Commands.StartCommand;
import org.bokoch.TelegramBot.Commands.StopCommand;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.commandbot.TelegramLongPollingCommandBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.logging.BotLogger;


public class CommandsHandler extends TelegramLongPollingCommandBot {

    private static final String LOGTAG = CommandsHandler.class.getName();
    //private static final ResourceBundle commands = ResourceBundle.getBundle("commands", Locale.getDefault());

    public CommandsHandler(String botUsername) {
        super(botUsername);

        HelpCommand helpCommand = new HelpCommand(this);
        register(helpCommand);
        register(new StartCommand());
        register(new StopCommand());
        registerDefaultAction((absSender, message) -> {
            SendMessage commandUnknownMessage = new SendMessage();
            commandUnknownMessage.setChatId(message.getChatId());
            commandUnknownMessage.setText("The command '" + message.getText() + "' is not known by this bot. Here comes some help " + Emoji.AMBULANCE);
            try {
                absSender.execute(commandUnknownMessage);
            } catch (TelegramApiException e) {
                BotLogger.error(LOGTAG, e);
            }
            helpCommand.execute(absSender, message.getFrom(), message.getChat(), new String[] {});
        });
    }
    @Override
    public void processNonCommandUpdate(Update update) {

        if (update.hasMessage()) {
            Message message = update.getMessage();


            if (message.hasText()) {
                SendMessage echoMessage = new SendMessage();
                echoMessage.setChatId(message.getChatId());
                echoMessage.setText("Hey heres your message:\n" + message.getText());

                try {
                    execute(echoMessage);
                } catch (TelegramApiException e) {
                    BotLogger.error(LOGTAG, e);
                }
            }
        }
    }

    @Override
    public String getBotToken() {
        return "464028994:AAEr5oA4cQ4SNaUq6Ks8lCe9zHB9dSAuYCc";
    }



}