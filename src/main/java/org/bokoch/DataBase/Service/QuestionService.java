package org.bokoch.DataBase.Service;

import org.bokoch.DataBase.Entity.QuestionEntity;

import java.util.List;

public interface QuestionService {
    public void insert(QuestionEntity question);
    public void delete(int id);
    public QuestionEntity update(QuestionEntity question);
    public QuestionEntity getQuestion(int id);
    public List<QuestionEntity> getAllQuestions();
}
