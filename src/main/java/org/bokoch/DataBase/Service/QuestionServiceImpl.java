package org.bokoch.DataBase.Service;

import org.bokoch.DataBase.Dao.QuestionDao;
import org.bokoch.DataBase.Entity.QuestionEntity;

import java.util.List;

public class QuestionServiceImpl implements QuestionService {

    private QuestionDao questionDao;

    @Override
    public void insert(QuestionEntity question) {
        questionDao.addQuestion(question);
    }

    @Override
    public void delete(int id) {
        questionDao.deleteQuestion(id);
    }

    @Override
    public QuestionEntity update(QuestionEntity question) {
        return questionDao.updateQuestion(question);
    }

    @Override
    public QuestionEntity getQuestion(int id) {
        return questionDao.getQuestion(id);
    }
    public List<QuestionEntity> getAllQuestions(){
        return questionDao.getAllQuestions();
    }

    public void setQuestionDao(QuestionDao questionDao) {
        this.questionDao = questionDao;
    }
}
