package org.bokoch.DataBase;

import org.bokoch.DataBase.Entity.AnswerEntity;
import org.bokoch.DataBase.Entity.QuestionEntity;
import org.bokoch.DataBase.Service.QuestionService;
import org.bokoch.DataBase.Service.QuestionServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;
import java.util.Scanner;


public class Main {

    private static Scanner mScanner = new Scanner(System.in);

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("/applicationContext.xml");
        QuestionService questionService = context.getBean(QuestionServiceImpl.class);

        start();
        int choice = 1;
        while (choice != 0) {
            choice = mScanner.nextInt();
            switch (choice) {
                case 1 : {
                    insert(questionService);
                    break;
                }
                case 2: {
                    getQuestions(questionService);
                    break;
                }
                case 3: {
                    delete(questionService);
                    break;
                }
                case 4: {
                    getQuestion(questionService);
                    break;
                }
                case 5: {
                    start();
                    break;
                }

                default: {
                    break;
                }
            }

        }
    }

    private static void start() {
        System.out.println("Привет, добавь в БазуДанных вопросы для бота\n" +
                "1 - добавить вопрос(Пример: What is better movie?)\n" +
                "2 - показать все вопросы с ответами\n" +
                "3 - удалить вопрос с ответом по id\n" +
                "4 - показать вопрос по id\n" +
                "5 - справка\n" +
                "0 - выход");
    }

    private static void insert(QuestionService service) {
        System.out.print("Введите вопрос: ");
        mScanner.nextLine();
        String question = mScanner.nextLine();
        QuestionEntity questionEntity = new QuestionEntity();
        questionEntity.setQuestion(question);
        System.out.println("Отлично, теперь введите ответы построчно, пустая строка закончить ввод");
        String answer;
        do {
            answer = mScanner.nextLine();
            if (answer!=null) {
                AnswerEntity answerEntity = new AnswerEntity();
                answerEntity.setAnswer(answer);
                questionEntity.addAnswer(answerEntity);
            }
        } while(!answer.isEmpty());
        service.insert(questionEntity);
    }

    private static void delete(QuestionService questionService) {
        int id = mScanner.nextInt();
        questionService.delete(id);
    }

    private static void getQuestion(QuestionService questionService) {
        int id = mScanner.nextInt();
        System.out.println(questionService.getQuestion(id));
    }

    private static void getQuestions(QuestionService questionService) {
        List<QuestionEntity> questionEntityList = questionService.getAllQuestions();
        for (QuestionEntity i : questionEntityList) {
            System.out.println(i);
        }
    }
}
