package org.bokoch.DataBase.Dao;

import org.bokoch.DataBase.Entity.QuestionEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class HibbernateQuestionDaoImpl implements QuestionDao {

    private SessionFactory sessionFactory;



    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    private Session currentSession() {
        return sessionFactory.getCurrentSession();
    }


    @Override
    @Transactional(readOnly = false)
    public void addQuestion(QuestionEntity question) {

        currentSession().merge(question);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteQuestion(int id) {
        QuestionEntity questionEntity = (QuestionEntity) sessionFactory.getCurrentSession().load(
                QuestionEntity.class, id);
        if (null != questionEntity) {
            this.sessionFactory.getCurrentSession().delete(questionEntity);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<QuestionEntity> getAllQuestions() {
        return currentSession().createQuery("from QuestionEntity").list();
    }

    @Override
    @Transactional(readOnly = false)
    public QuestionEntity updateQuestion(QuestionEntity question) {
        currentSession().update(question);
        return question;
    }

    @Override
    @Transactional(readOnly = true)
    public QuestionEntity getQuestion(int id) {
        return (QuestionEntity) currentSession().get(QuestionEntity.class, id);
    }
}
