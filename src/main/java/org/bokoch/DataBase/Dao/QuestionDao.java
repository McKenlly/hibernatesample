package org.bokoch.DataBase.Dao;

import org.bokoch.DataBase.Entity.QuestionEntity;

import java.util.List;

public interface QuestionDao {

    public void addQuestion(QuestionEntity question);
    public void deleteQuestion(int id);
    public List<QuestionEntity> getAllQuestions();
    public QuestionEntity updateQuestion(QuestionEntity question);
    public QuestionEntity getQuestion(int id);

}
