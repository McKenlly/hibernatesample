package org.bokoch.DataBase.Entity;


import javax.persistence.*;

@Entity
@Table(name="ANSWERS", schema = "quiz_bot_db")
public class AnswerEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name = "answer", nullable = false)
    private String answer;


    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="question_id", referencedColumnName = "id")
    private QuestionEntity question;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public QuestionEntity getQuestion() {
        return question;
    }

    public void setQuestion(QuestionEntity question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return String.format("[id = %s, answer = %s]", id, answer);
    }

}
