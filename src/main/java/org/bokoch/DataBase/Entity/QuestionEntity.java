package org.bokoch.DataBase.Entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "QUESTIONS", schema = "quiz_bot_db")
public class QuestionEntity {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name = "question", nullable = false)
    private String question;

    @Column(name = "correct_answer", nullable = false)
    private int correctAnswer;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private Set<AnswerEntity> answerEntities = new HashSet<AnswerEntity>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Set<AnswerEntity> getAnswerEntities() {
        return answerEntities;
    }

    public void setAnswerEntities(Set<AnswerEntity> answerEntities) {
        this.answerEntities = answerEntities;
    }

    public void addAnswer(AnswerEntity answerEntity) {
        answerEntity.setQuestion(this);
        answerEntities.add(answerEntity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionEntity that = (QuestionEntity) o;

        if (id != that.id) return false;
        if (correctAnswer != that.correctAnswer) return false;
        if (question != null ? !question.equals(that.question) : that.question != null) return false;


        return true;
    }

    @Override
    public String toString() {
        return String.format("[id = %s, question = %s, Answers [ %s ] ]", id, question, answerEntities);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (question != null ? question.hashCode() : 0);
        result = 31 * result + correctAnswer;
        return result;
    }
}
