CREATE TABLE ANSWERS
(
  id INT NOT NULL AUTO_INCREMENT
  , question_id int not null
  , answer VARCHAR(60) NOT NULL
  , UNIQUE UQ_ANSWER_1 (answer)
  , PRIMARY KEY (ID)
  , CONSTRAINT FK_ANSWER_2 FOREIGN KEY (question_id) references QUESTIONS(id)
);

CREATE TABLE QUESTIONS (
  id int not null auto_increment,
  question varchar(255) not null,
  correct_answer int not null ,
  unique  UQ_QUESTION_1 (id, question),
  primary key (id)
)

